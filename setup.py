from setuptools import setup, find_packages

setup(
    name='mattermost-cas',
    version='0.1',

    description=('Flask application as GitLab OAuth2 provider for Mattermost '
                 'login with CAS'),
    long_description=('A Flask application mimicking GitLab to serve as a '
                      'bridge for OAuth2 <-> CAS authentication with '
                      'Mattermost'),
    url='https://git.eleves.ens.fr/cof-geek-sysadmin/mattermost-cas',

    author='tobast',
    author_email='contact@tobast.fr',

    license='MIT',

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    keywords='flask mattermost oauth2 cas single-sign-on sso',

    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),

    install_requires=['sqlalchemy', 'flask', 'flask-sqlalchemy',
                      'flask-login', 'oauthlib==1.1.2', 'Flask-OAuthlib',
                      'psycopg2', 'Flask-CAS'],

    extras_require={
        'dev': ['check-manifest'],
    },
)
