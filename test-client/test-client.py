#!/usr/bin/env python3

import os
from flask import Flask, redirect, url_for, session
from werkzeug.contrib.fixers import ProxyFix
from local_gitlab import make_local_gitlab_blueprint, local_gitlab

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
app.secret_key = "supersekrit"
app.config['DEBUG'] = True
app.config['SESSION_COOKIE_NAME'] = 'session_test_client'
# Without this ^, the actual mattermost_cas.py overwrites the session cookie
# since cookies are domain-isolated but not port-isolated

blueprint = make_local_gitlab_blueprint(
    client_id='test_client_id',
    client_secret='test_client_secret',
    scope='login',
    redirect_url='http://localhost:5001/',
    hostname='localhost:5000',
)
app.register_blueprint(blueprint, url_prefix="/login")


@app.route("/")
def index():
    if not local_gitlab.authorized:
        return redirect(url_for("gitlab.login"))
    resp = local_gitlab.get("/api/v4/user")
    print(resp)
    assert resp.ok
    print(resp.text)
    return "You are {} on GitLab bridge".format(resp.text)


@app.route("/logout")
def logout():
    session.clear()
    return "ok"


if __name__ == "__main__":
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    app.run(host='0.0.0.0', port=5001)
