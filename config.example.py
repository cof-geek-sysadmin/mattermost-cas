''' Configuration module '''

DEBUG = False  # NEVER set to True in production

SECRET_KEY = 'CHANGE_ME'  # Generate using eg. `pwgen 60 1`

ID_DERIVATION_SHARED_SECRET = 'CHANGE_ME'  # Generate using eg. `pwgen 60 1`
# This key ^ must be shared with the user creation scripts. It is used to
# derive the user id from the username

WSGI_APP = 'gunicorn'  # Set to None otherwise

OAUTH2_PROVIDER_ERROR_ENDPOINT = 'error_view'
SESSION_COOKIE_NAME = 'mattermost_cas_session'

SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:root@localhost/example_db'
SQLALCHEMY_TRACK_MODIFICATIONS = False

CLIENTS = [
    {
        'root_url': 'https://mattermost.example.com',
        'client_id': 'example_client_id',
        'client_secret': 'example_client_secret',
        'default_scope': 'login',
        'redirect_uris': [
            "signup/gitlab/complete",
            "login/gitlab/complete",
            "login/gitlab/authorized"],
        'default_redirect_uri': 'signup/gitlab/complete',
    },
]

if DEBUG:  # You'll have to regenerate the DB for it to work
    CLIENTS.append({
        'root_url': 'http://localhost:5001',
        'client_id': 'test_client_id',
        'client_secret': 'test_client_secret',
        'default_scope': 'login',
        'redirect_uris': [
            "signup/gitlab/complete",
            "login/gitlab/complete",
            "login/gitlab/authorized"],
        'default_redirect_uri': 'signup/gitlab/complete',
    })


CAS_SERVER = 'https://cas.example.com/'
CAS_AFTER_LOGIN = 'index'

LDAP_CACHE_HOURS = 72
LDAP_CONFIG = {
    'server': 'ldap.example.com',
    'port': 636,
    'debug_level': 255,
    'network_timeout': 10,
    'timeout': 10,
}
