from clipper_ldap import ClipperLDAP
from flask import current_app
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.hybrid import hybrid_property
import hmac
import datetime
from functools import wraps

db = SQLAlchemy()


class Client(db.Model):
    __tablename__ = "clients"

    client_id = db.Column(db.String(40), primary_key=True)
    client_secret = db.Column(db.String(55), unique=True, index=True, nullable=False)
    _redirect_uris = db.Column(db.Text)
    default_redirect_uri = db.Column(db.Text)
    _default_scopes = db.Column(db.Text)

    def __init__(self, client_id, client_secret, redirect_uris, default_redirect_uri, default_scopes):
        self.client_id = client_id
        self.client_secret = client_secret
        self.redirect_uris = redirect_uris
        self.default_redirect_uri = default_redirect_uri
        self.default_scopes = default_scopes

    @property
    def client_type(self):
        return 'public'

    @hybrid_property
    def redirect_uris(self):
        if self._redirect_uris:
            return self._redirect_uris.split()
        return []

    @redirect_uris.setter
    def redirect_uris(self, value):
        self._redirect_uris = " ".join(value)

    @hybrid_property
    def default_scopes(self):
        if self._default_scopes:
            return self._default_scopes.split()
        return []

    @default_scopes.setter
    def default_scopes(self, value):
        self._default_scopes = " ".join(value)


class PublicIdExists(Exception):
    pass


def ldap_dependant(fun):
    ''' This function relies on LDAP-cached attributes '''
    @wraps(fun)
    def wrap(self, *args, **kwargs):
        self._update_ldap()
        return fun(self, *args, **kwargs)
    return wrap


class User(db.Model, UserMixin):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), nullable=False)
    public_id = db.Column(db.BigInteger, nullable=False)
    cached_name = db.Column(db.String(128), nullable=True)
    last_ldap_update = db.Column(db.DateTime(), nullable=True)

    def __init__(self, username):
        self.username = username
        self._compute_public_id()
        if self.check_public_id_exists():
            raise PublicIdExists(
                "The public ID {} for user {} already exists!".format(
                    self.public_id, self.username)
                )

    def check_public_id_exists(self):
        ''' Checks that no other user has the same public_id. Intended for
        creation time, before the user is committed '''

        return self.query.filter_by(public_id=self.public_id).count() > 0

    @staticmethod
    def compute_public_id(username):
        ''' Derives the user data (pseudo-gitlab id) from its username

        The auth data *must* be an integer fitting into a Go int64 datatype
        (source: mattermost:model/gitlab/gitlab.go). Assuming a total of about
        2000 users of this server, there is a probability of about 1e-57 of
        collision, which is acceptable.
        '''

        derivation_key = \
            current_app.config['ID_DERIVATION_SHARED_SECRET'].encode('utf-8')
        hex_string = hmac.new(
            derivation_key,
            username.encode('utf-8')).hexdigest()
        hex_value = int(hex_string, base=16)
        fitting_threshold = 2 ** 63 - 1
        identifier = hex_value % fitting_threshold

        return identifier

    def _ldap_expired(self):
        if not self.last_ldap_update:
            return True  # Never fetched yet
        cur_datetime = datetime.datetime.now()
        time_delta = datetime.timedelta(
            hours=current_app.config['LDAP_CACHE_HOURS'])
        return self.last_ldap_update + time_delta < cur_datetime

    def _update_ldap(self):
        ''' Updates the LDAP-cached properties for the current user '''
        if not self._ldap_expired():
            current_app.logger.debug("LDAP still live")
            return  # No need to update

        current_app.logger.debug("Updating LDAP")
        ldap = ClipperLDAP()
        user_ldap = ldap.fetch_username(self.username)
        current_app.logger.debug("Got user_ldap={}".format(user_ldap))
        if user_ldap is not None:
            self.cached_name = user_ldap.name
            self.last_ldap_update = datetime.datetime.now()

    @property
    @ldap_dependant
    def name(self):
        ''' User's real name from the LDAP directory '''
        return self.cached_name

    def _compute_public_id(self):
        public_id = self.compute_public_id(self.username)
        self.public_id = public_id


del ldap_dependant


class GrantToken(db.Model):
    __tablename__ = "grant_tokens"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id, ondelete='CASCADE'))
    user = db.relationship(User)
    client_id = db.Column(db.String(40), nullable=False)
    code = db.Column(db.String(255), index=True, nullable=False)

    redirect_uri = db.Column(db.String(255))
    expires = db.Column(db.DateTime)

    _scopes = db.Column(db.Text)

    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

    @property
    def scopes(self):
        if self._scopes:
            return self._scopes.split()
        return []


class BearerToken(db.Model):
    __tablename__ = "bearer_tokens"

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.String(40), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)

    token_type = db.Column(db.String(40))

    access_token = db.Column(db.String(255), unique=True)
    refresh_token = db.Column(db.String(255), unique=True)
    expires = db.Column(db.DateTime)
    _scopes = db.Column(db.Text)

    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

    @property
    def scopes(self):
        if self._scopes:
            return self._scopes.split()
        return []
