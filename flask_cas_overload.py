''' Overlaod a few features of flask-cas '''

from functools import wraps

import flask
from flask_cas import login


def login_required(function):
    @wraps(function)
    def wrap(*args, **kwargs):
        if 'CAS_USERNAME' not in flask.session:
            flask.session['CAS_AFTER_LOGIN_SESSION_URL'] = \
                flask.request.script_root + flask.request.full_path
            return login()
        else:
            return function(*args, **kwargs)
    return wrap
