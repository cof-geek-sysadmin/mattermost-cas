#!/usr/bin/env python3

import datetime
import json
import argparse
import os
import logging

from flask import Flask, request, abort, session, jsonify, render_template
from flask_oauthlib.provider import OAuth2Provider

from flask_cas import CAS
from flask_cas_overload import login_required

from models import db, GrantToken, BearerToken, Client, User

app = Flask(__name__)
app.config.from_pyfile('config.py')

CAS(app)
db.init_app(app)
oauth = OAuth2Provider(app)


def get_current_user():
    ''' Get the currently logged-in user '''

    def get_user_or_create(username):
        ''' Gets the user from database, or creates it '''
        user = User.query.filter_by(username=username).first()
        if not user:
            user = User(username)
            db.session.add(user)
            db.session.commit()

        return user

    if 'CAS_USERNAME' not in session:
        raise Exception("User not logged in")
    return get_user_or_create(session['CAS_USERNAME'])


# FIXME maybe delete this?
@app.route("/api/v3/user")
@oauth.require_oauth("login")
def sso_user_info_for_mattermost():
    user = request.oauth.user
    return json.dumps({"id": user.id,
                       "username": user.username,
                       "login": user.username,
                       "email": user.email,
                       "name": user.username,
                       })


@oauth.clientgetter
def load_client(requested_client_id):
    return Client.query.filter_by(client_id=requested_client_id).first()


@oauth.grantgetter
def load_grant(client_id, code):
    return GrantToken.query.filter_by(client_id=client_id, code=code).first()


@oauth.grantsetter
def save_grant(client_id, code, request):
    expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=100)
    grant = GrantToken(
        client_id=client_id,
        code=code['code'],
        redirect_uri=request.redirect_uri,
        _scopes=' '.join(request.scopes),
        user=get_current_user(),
        expires=expires,
    )
    db.session.add(grant)
    db.session.commit()

    return grant


@oauth.tokengetter
def load_token(access_token=None, refresh_token=None):
    if access_token:
        return BearerToken.query.filter_by(access_token=access_token).first()
    elif refresh_token:
        return BearerToken.query.filter_by(refresh_token=refresh_token).first()


@oauth.tokensetter
def save_token(token, _request):
    existing_tokens = BearerToken.query.filter_by(
        client_id=_request.client.client_id,
        user_id=_request.user.id
    )
    # make sure that every client has only one token connected to a user
    for existing_bearer_token in existing_tokens:
        db.session.delete(existing_bearer_token)
    expires_in = token.pop('expires_in')
    expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=expires_in)

    new_token = BearerToken(
        access_token=token['access_token'],
        refresh_token=token['refresh_token'],
        token_type=token['token_type'],
        _scopes=token['scope'],
        expires=expires,
        client_id=_request.client.client_id,
        user_id=_request.user.id,
    )
    db.session.add(new_token)
    db.session.commit()
    return new_token


@app.route('/oauth/authorize', methods=['GET', 'POST'])
@login_required
@oauth.authorize_handler
def authorize(*args, **kwargs):
    return True


@app.route('/oauth/token', methods=["GET", "POST"])
@oauth.token_handler
def access_token():
    return None


@app.before_first_request
def create_database():
    db.create_all()
    if not Client.query.count():
        for client in app.config['CLIENTS']:
            url_fmt = client['root_url'] + "/{}"
            db.session.add(Client(
                client_id=client['client_id'],
                client_secret=client['client_secret'],
                redirect_uris=list(map(
                    lambda x: url_fmt.format(x),
                    client['redirect_uris'])),
                default_redirect_uri=url_fmt.format(
                    client['default_redirect_uri']),
                default_scopes=[client['default_scope']]
            ))
    db.session.commit()


@app.route("/")
def front_page():
    abort(404)


@app.route("/error")
def error_view():
    def get_dft(src, key, default=None):
        if key in src:
            return src[key]
        return default

    error = get_dft(request.args, 'error', default='An error occurred')
    error_descr = get_dft(request.args, 'error_description',
                          default='Somehow, something bad occurred.')
    return render_template('error.html', **{
        'error': error,
        'error_description': error_descr,
    })


@app.route('/api/v4/user')
@oauth.require_oauth()
def user():
    user = request.oauth.user
    return jsonify({
        'id': user.public_id,
        'name': user.name,
        'username': user.username,
        'state': 'active',
        'email': '{}@clipper.ens.fr'.format(user.username),
        })


def parse_args():
    ''' Parse command-line arguments '''
    parser = argparse.ArgumentParser(description='OAuth <-> CAS bridge')
    parser.add_argument('--local-testing', '-g', action='store_true',
                        help=('Enable local testing mode: use a '
                              'locally-generated SSL certificate with testing '
                              'ROOT_URL, CLIENT_ID, CLIENT_SECRET and '
                              'DEFAULT_SCOPE values, independant from '
                              'configuration.'))

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    if args.local_testing:
        os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
        app.run(host='0.0.0.0')

    else:
        app.run(host="0.0.0.0")
else:
    if app.config['WSGI_APP'] == 'gunicorn':
        gunicorn_logger = logging.getLogger('gunicorn.error')
        app.logger.handlers = gunicorn_logger.handlers
        app.logger.setLevel(gunicorn_logger.level)
